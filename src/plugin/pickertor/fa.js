export default {
    name: "fa",
    weekDays: [
        "شنبه",
        "دوشنبه",
        "يكشنبه",
        "سه شنبه",
        "چهارشنبه",
        "پنج شنبه",
        "جمعه",
    ],
    shortWeekDays: [
        "ش",
        "ی",
        "د",
        "س",
        "چ",
        "پ",
        "ج",
    ],
    monthsName: ["فروردين", "ارديبهشت", "خرداد", "تير", "مرداد", "شهريور", "مهر", "آبان", "آذر", "دي", "بهمن", "اسفند"],
    word: {
        "switch": "EN",
        today: "امروز",
        next: "بعد",
        perv: "قبل",
        weekly:"هفتگی",
        monthly:"ماهانه" ,
        yearly:"سالانه" ,
        daily:"روزانه" ,
    }
}
