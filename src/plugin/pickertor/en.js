export default {
    name: "en",
    weekDays: [
        'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ],
    shortWeekDays: [
        'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'
    ],
    monthsName:
        ["January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"],
    shortMonthsName: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    word: {
        switch: "FA",
        today: "today",
        next: "next",
        perv: "perv",
        weekly:"weekly",
        monthly:"monthly" ,
        yearly:"yearly" ,
        daily:"daily" ,
    }
}
