let moment = require('jalali-moment')
import fa from "./fa"
import en from "./en"

const localMethods = {
    fa: {
        isBefore: "isSameOrBefore",
        local: fa,
        dir: "rtl"
    },
    en: {
        isBefore: "isBefore",
        local: en,
        dir: "ltr"
    }
}

class PickerTor {
    constructor() {
        this.m = moment
        this.locale = "fa"
        this.m.locale(this.locale)
        this.activeMonth = null
        this.mode = "month"
        // this.generateDates({}) // this.m("2019/06/01") || this.m("1399/06/01")
    }

    generateDates({monthToShow = this.m(), locale = this.locale, changeMode = false}) {
        if (!this.m.isMoment(monthToShow)) return null;
        this.m.locale(locale);
        this.locale = locale
        this.activeMonth = monthToShow
        let method = localMethods[locale]
        let startDay;
        let endDay;
        switch (this.mode) {
            case "day":
                startDay = this.m(monthToShow).startOf("day")
                endDay = this.m(monthToShow).endOf("day")
                break;
            case "week":
                startDay = this.m(monthToShow).startOf("week")
                endDay = this.m(monthToShow).endOf("week");
                break;
            case "year":
                startDay = this.m(monthToShow).startOf("year").startOf("week")
                endDay = this.m(monthToShow).endOf("year").endOf("week");
                break;
            default:
                startDay = this.m(monthToShow).startOf("month").startOf("week")
                endDay = this.m(monthToShow).endOf("month").endOf("week")
        }
        let day = startDay.clone().subtract(1, "day")
        if (changeMode && ["month"].includes(this.mode)) {
            day = startDay.clone().subtract(this.locale === "fa" ? 1 : 0, "day")
        }
        const calender = []
        if (!["day"].includes(this.mode)) {
            while (day[method.isBefore](endDay, "day")) {
                calender.push(
                    Array(7).fill(0)
                        .map(() => day.add(1, "day").clone())
                )
            }
            if (calender.length < 6 && ["month"].includes(this.mode)) {
                calender.push(
                    Array(7).fill(0)
                        .map(() => day.add(1, "day").clone())
                )
            }
        } else {
            calender.push([day.add(1, "day").clone()])
        }
        return calender
    }

    setConfig({mode = this.mode, locale = this.locale}) {
        this.locale = locale
        this.mode = mode
    }

    next() {
        switch (this.mode) {
            case "day":
                return this.generateDates({monthToShow: this.activeMonth.clone().add(1, "day")})
            case "week":
                return this.generateDates({monthToShow: this.activeMonth.clone().add(7, "day")})
            case "year":
                return this.generateDates({monthToShow: this.activeMonth.clone().add(1, "year")})
            default:
                return this.generateDates({monthToShow: this.activeMonth.clone().add(1, "month")})
        }
    }

    perv() {
        switch (this.mode) {
            case "week":
                return this.generateDates({monthToShow: this.activeMonth.clone().subtract(1, "day")})
            case "day":
                return this.generateDates({monthToShow: this.activeMonth.clone().subtract(7, "day")})
            case "year":
                return this.generateDates({monthToShow: this.activeMonth.clone().subtract(1, "year")})
            default:
                return this.generateDates({monthToShow: this.activeMonth.clone().subtract(1, "month")})
        }
    }

    thisMonth() {
        return this.activeMonth
    }

    activeLocal() {
        return localMethods[this.locale]
    }


    isSelected(day, value) {
        if (!value) return false
        return value.isSame(day, "day");
    }

    beforeToday(day) {
        return day.isBefore(this.m(), "day");
    }

    beforeMonth(day) {
        return !["day", "week", "year"].includes(this.mode) ? day.month() !== this.activeMonth.month() : false
    }
    beforeYear(day) {
        return ["year"].includes(this.mode) ? day.year() !== this.activeMonth.year() : false
    }

    isToday(day) {
        return day.isSame(this.m(), "day")
    }

    today() {
        return this.m()
    }

    dayStyles(day, value) {
        // if (this.beforeToday(day)) return "before-today";
        if (this.beforeMonth(day)) return "before-month";
        if (this.beforeYear(day)) return "before-month";
        if (this.isSelected(day, value)) return "selected";
        if (this.isToday(day)) return "today";
        return "";
    }
}

export default PickerTor
